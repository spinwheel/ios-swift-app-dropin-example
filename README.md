# Explore WKScriptMessageHandler

[![Build Status](https://travis-ci.org/chicio/Explore-WKScriptMessageHandler.svg?branch=master)](https://travis-ci.org/chicio/Explore-WKScriptMessageHandler)
[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/chicio/React-Native-Native-Modules-Communication/master/LICENSE.md)
[![Supported platform](https://img.shields.io/badge/platforms-iOS-orange.svg)](https://img.shields.io/badge/platforms-iOS-orange.svg)

### Purpose

An example project in which we explore how it is possible to use `WKScriptMessageHandler` to make some `WKWebView` web page to native communication.  
Here is a blog post for reference: [Web to native communication on iOS using WKScriptMessageHandler](https://www.fabrizioduroni.it/2019/08/03/html-javascript-to-native-communication-ios.html "Web to native communication on iOS using WKScriptMessageHandler") 


# Prerequisite needed

Install [Xcode](https://developer.apple.com/xcode/  "Xcode") onto your system.


# Getting Started
## Clone

To begin, open a terminal window and clone this repository onto your local machine using the command:

```
git clone git@bitbucket.org:spinwheel/ios-swift-app-dropin-example.git
```

Once cloned, cd in the directory containing like so:

```
cd ios-swift-app-dropin-example/ExploreWKScriptMessageHandler 
```
The following directory ExploreWKScriptMessageHandler contains a form.html file.


## Serve the form html file

One way of doing this would be to use the following npm packages.
Globally install npm packages: "serve" and "localtunnel"

Serve the html file to a port and run localtunnel for the same port
```
npx serve .
npx localtunnel --port 3000
```

Once local tunnel is successfully running you get a dynamic url like this one.
Run it in the browser and give necessary permissions. Also check if form.html is accessible.
```
https://quick-chipmunk-44.loca.lt
```


## Open the project on Xcode 

Open the file: FormViewController.swift

Replace the url placeholder inside the loadPage function with the actual url that serves the form.html file.
```
 let url = URL(string: "https://quick-chipmunk-44.loca.lt/form.html")
```

Generate a new [token](https://api-docs.spinwheel.io/v1/doc/#/Drop-in%20Module/dropInModuleToken "token") and replace it inside the window.init function.
You can add the dropin name and additional configuration if any is required.
Token and module (dropin name) being mandatory.

```
window.init({ module: 'loan-servicers-login', token:'<PASS-TOKEN-HERE>', additionaParam1: additionalValue1, ...})
```
Press the play button to create a new build and start the emulator.



IMPORTANT: For better understanding of Spinwheel dropins and all the configurations we offer, please visit our [developer portal]( https://developer.spinwheel.io/ "developer portal")

You can also refer our [demo app](https://bitbucket.org/spinwheel/swsl-dropin-js-module-demo/src/master/README.md "demo app") to understand how we can invoke the Spiwnheel dropins. We have used the same script to invoke the dropin via the form.html file in this repository.


# Using the Application

Once the emulator starts, click the ExploreWKS to render the dropin if not already running.
Use Safari dev tools for simulator to track network calls and logs.
