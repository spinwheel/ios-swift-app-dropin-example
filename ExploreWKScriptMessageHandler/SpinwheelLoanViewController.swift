import UIKit
import WebKit
import ReactiveSwift
import ReactiveCocoa
import ServiceDefinitions
import ViewSupport

protocol SpinwheelLoanViewControllerDelegate: AnyObject {
    func didTapExit(in viewController: UIViewController)
}

final class SpinwheelLoanViewController: UIViewController {
    typealias ViewModel = SpinwheelLoanViewModel

    private lazy var webView: WKWebView = makeWebView()
    private lazy var loader: UIActivityIndicatorView = UIActivityIndicatorView()
    private lazy var closeButton: UIButton = makeCloseButton()

    private let metricsTracker: MetricsTracker
    private unowned let delegate: SpinwheelLoanViewControllerDelegate
    private let viewModel: ViewModel

    init(
        viewModel: ViewModel,
        metricsTracker: MetricsTracker,
        delegate: SpinwheelLoanViewControllerDelegate
    ) {
        self.metricsTracker = metricsTracker
        self.viewModel = viewModel
        self.delegate = delegate

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        disableZoom()
        fetchToken()

        navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .qpl_grey100
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.shouldShowDashboard ? metricsTracker.track(.spinwheelDashboardView) : metricsTracker.track(.spinwheelConnectView)
    }

    private func fetchToken() {
        viewModel.loadTokenAction.apply().start()

        viewModel.loadTokenAction.values
            .take(duringLifetimeOf: self)
            .observeValues { [unowned self] data in
                webView.evaluateJavaScript(
                    "init({\"environment\": \"\(data.environment)\", \"token\": \"\(data.token)\", \"dashboard\": \(viewModel.shouldShowDashboard)});"
                )
            }
    }

    private func setup() {
        view.addSubview(webView)
        webView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
            $0.top.equalTo(view.safeAreaLayoutGuide)
        }

        loader.startAnimating()
        view.addSubview(loader)
        loader.snp.makeConstraints {
            $0.center.equalToSuperview()
        }

        view.addSubview(closeButton)
        closeButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(16)
            $0.top.equalTo(view.safeAreaLayoutGuide).offset(16)
            $0.width.height.equalTo(32)
        }

        closeButton.reactive
            .controlEvents(.touchUpInside)
            .take(during: reactive.lifetime)
            .observeValues { [unowned self] _ in
                delegate.didTapExit(in: self)
        }

        webView.load(URLRequest(url: URL(string: "https://static.qapitalapp.net/assets/ios.api.qapital.com/spinwheel/v1/ios.html")!)) // swiftlint:disable:this force_unwrapping
    }

    private func disableZoom() {
        let source: String = "var meta = document.createElement('meta');" +
            "meta.name = 'viewport';" +
            "meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';" +
            "var head = document.getElementsByTagName('head')[0];" +
            "head.appendChild(meta);"

        let script: WKUserScript = WKUserScript(
            source: source,
            injectionTime: .atDocumentEnd,
            forMainFrameOnly: true
        )

        webView.configuration.userContentController.addUserScript(script)
    }

}

// MARK: WKNavigationDelegate

extension SpinwheelLoanViewController: WKNavigationDelegate {

    // Removes text selection, copy, paste, share feature on long press
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.documentElement.style.webkitUserSelect='none'")
        webView.evaluateJavaScript("document.documentElement.style.webkitTouchCallout='none'")
        webView.evaluateJavaScript("var elems = document.getElementsByTagName('a'); for (var i = 0; i < elems.length; i++) { elems[i]['href'] = 'javascript:(void)'; }")
    }
}

// MARK: WKScriptMessageHandler

extension SpinwheelLoanViewController: WKScriptMessageHandler {

    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "onLoad" {
            loader.stopAnimating()
            closeButton.isHidden = true
        }

        if message.name == "onExit" {
            delegate.didTapExit(in: self)
        }

        if message.name == "onSuccess" {
            metricsTracker.track(.spinwheelLoanConnected)
            viewModel.loanAddedAction.apply().start()
        }

        if message.name == "onError" {
            debugPrint(message.body)
        }
        if message.name == "onEvent" {
            debugPrint(message.body)
        }
    }

}

extension SpinwheelLoanViewController {

    private func makeWebView() -> WKWebView {
        let preferences = WKPreferences()
        preferences.javaScriptCanOpenWindowsAutomatically = true
        preferences.javaScriptEnabled = true
        let configuration = WKWebViewConfiguration()
        configuration.userContentController.add(self, name: "onLoad")
        configuration.userContentController.add(self, name: "onExit")
        configuration.userContentController.add(self, name: "onSuccess")
        configuration.userContentController.add(self, name: "onError")
        configuration.userContentController.add(self, name: "onEvent")
        configuration.preferences = preferences
        let webview = WKWebView(frame: .zero, configuration: configuration)
        webview.backgroundColor = .white
        webview.navigationDelegate = self
        webview.scrollView.alwaysBounceVertical = false
        webview.scrollView.bounces = false
        return webview
    }

    private func makeCloseButton() -> UIButton {
        let view = UIButton()
        view.setImage(Asset.Icon.closeIcon.image, for: .normal)
        return view
    }

}
