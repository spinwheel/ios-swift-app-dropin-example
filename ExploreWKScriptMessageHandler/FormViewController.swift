//
//  FormViewController.swift
//  ExploreWKScriptMessageHandler
//

import UIKit
import WebKit

class FormViewController: UIViewController, WKScriptMessageHandler {
    private var wkWebView: WKWebView!

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        self.setupWKWebview()
        self.loadPage()
    }

    private func setupWKWebview() {
        self.wkWebView = WKWebView(frame: self.view.bounds, configuration: self.getWKWebViewConfiguration())
        self.view.addSubview(self.wkWebView)
    }
    
    private func loadPage() {
            let url = URL(string: "<local-tunnel-url>/form.html")

            self.wkWebView.load( URLRequest(url: url!))
            
        
    }
    
    private func getWKWebViewConfiguration() -> WKWebViewConfiguration {
        let userController = WKUserContentController()
        userController.add(self, name: "onReadyState")
        userController.add(self, name: "onDOMContentLoaded")
        userController.add(self, name: "onExit")
        userController.add(self, name: "onLoad")
        let configuration = WKWebViewConfiguration()
        configuration.userContentController = userController
        return configuration
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        let messageBody = message.body as? String;
        if(messageBody   ==  "loaded") {

            //Pass token and module below. Add any additional configurations beside token if required.
            self.wkWebView.evaluateJavaScript("window.init({ module: '<DROPIN-NAME>', token:'<PASS-TOKEN-HERE>'})") { (result, error) in
                if error == nil {
                    print(result)
                }
            }
            
        } else {
            print(messageBody)
        }
    }
    
    private func showUser(email: String, name: String) {
        let userDescription = "\(email) \(name)"
        let alertController = UIAlertController(title: "User", message: userDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        present(alertController, animated: true)
    }
}
